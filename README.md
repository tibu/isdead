# Everything is dead

Project page: https://gitlab.com/tibu/isdead

Local development:

```bash
git clone https://gitlab.com/tibu/isdead
cd isdead
npm install live-server
chrome --host-rules="MAP *.isdead.fyi 127.0.0.1:8080"
```